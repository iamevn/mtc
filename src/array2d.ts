import { NumberDictionary } from './types';

// 1 indexed 2D array internally represented by 1D array.
// do not change width/height after creating, make a new copy and fill in as desired instead
// iterate over entries with
//    for _, e in Array2D:entries() do ... end
//
//   w=4, h=3
//    1  2  3  4
//    5  6  7  8
//    9 10 11 12
export class Array2D<T> {
  public _storage: NumberDictionary<T>;
  public width: number;
  public height: number;

  constructor(width: number, height: number) {
    this.width = width;
    this.height = height;
    this._storage = {};
  }

  public _coordToIndex(x: number, y: number): number {
    return x + (y - 1) * this.width;
  }

  public _indexToCoord(index: number): LuaMultiReturn<[number, number]> {
    let x = index % this.width;
    if (x == 0) {
      x = 4;
    }
    return $multi(x, math.floor((index - 1) / this.width) + 1);
  }

  // returns false and doesn't insert if x, y out of bounds
  // returns inserted element otherwise
  public put(x: number, y: number, element: T): T | false {
    if (this.inBounds(x, y)) {
      this._storage[this._coordToIndex(x, y)] = element;
      return element;
    } else {
      return false;
    }
  }

  public remove(x: number, y: number): T {
    const idx = this._coordToIndex(x, y);
    const tmp = this._storage[idx];
    delete this._storage[idx];
    return tmp;
  }

  public get(x: number, y: number): T {
    return this._storage[this._coordToIndex(x, y)];
  }

  public getRow(y: number): NumberDictionary<T> {
    const tmp = {};
    for (const i of $range(1, this.width)) {
      tmp[i] = this.get(i, y);
    }
    return tmp;
  }

  public getColumn(x: number): NumberDictionary<T> {
    const tmp = {};
    for (const i of $range(1, this.height)) {
      tmp[i] = this.get(x, i);
    }
    return tmp;
  }

  public inBounds(x: number, y: number): boolean {
    return x > 0 && x <= this.width && y > 0 && y <= this.height;
  }

  public isFilled(x: number, y: number): boolean {
    return this._coordToIndex(x, y) in this._storage;
  }

  public isEmpty(x: number, y: number): boolean {
    return !this.isFilled(x, y);
  }

  // public entries(): LuaIterable<LuaMultiReturn<[keyof T, T[keyof T]]>> {
  public entries(): LuaIterable<LuaMultiReturn<[number, T]>, undefined> {
    return pairs(this._storage);
  }

  public clear(): void {
    this._storage = {};
  }
}

// 0 indexed array. see Array2D for full api
//
//   w=4, h=3
//    0  1  2  3
//    4  5  6  7
//    8  9 10 11
export class Array2DZ<T> extends Array2D<T> {
  public _coordToIndex(x: number, y: number): number {
    return x + y * this.width;
  }

  public _indexToCoord(index: number): LuaMultiReturn<[number, number]> {
    return $multi(index % this.width, math.floor(index / this.width));
  }

  public getRow(y: number): NumberDictionary<T> {
    const tmp = {};
    for (const i of $range(0, this.width - 1)) {
      tmp[i] = this.get(i, y);
    }
    return tmp;
  }

  public getColumn(x: number): NumberDictionary<T> {
    const tmp = {};
    for (const i of $range(0, this.height - 1)) {
      tmp[i] = this.get(x, i);
    }
    return tmp;
  }

  public inBounds(x: number, y: number): boolean {
    return x >= 0 && x < this.width && y >= 0 && y < this.height;
  }
}

// would a resize method be useful?
