import type { SpriteBatch } from 'love.graphics';

import type { ColorName } from './colors';
import { Colors } from './colors';
import type { CoordinateLayout, Rotation, StringDictionary } from './types';

// Resource namespace so that it only loads once when first imported.
// Future loads will reuse the same existing namespace.

// eslint-disable-next-line @typescript-eslint/no-namespace
namespace Resources {
  export const res = {
    block_img: love.graphics.newImage('res/block.png'),
    counter_img: love.graphics.newImage('res/counter.png'),
    overflow_img: love.graphics.newImage('res/overflow.png'),
    block_sprites: {} as StringDictionary<SpriteBatch>,
    piece_sprites: [] as { pieces: SpriteBatch[]; ghosts: SpriteBatch[] }[],

    layouts: {
      normal: [] as CoordinateLayout[],
      attack: [] as CoordinateLayout[],
      counter: [] as CoordinateLayout[],
      overflow: [] as CoordinateLayout[],
    },

    getBlockSprite: (colorname: ColorName): SpriteBatch => {
      // Reuse the same spritebatch for each type of colored block.
      if (res.block_sprites[colorname]) {
        return res.block_sprites[colorname];
      } else {
        const spr = love.graphics.newSpriteBatch(res.block_img);
        spr.setColor(...Colors.get(colorname));
        res.block_sprites[colorname] = spr;
        return spr;
      }
    },

    getPieceSprites: (
      piecename: string,
      colorname: ColorName,
      rotations: Rotation[],
      board_grid_size: number
    ): LuaMultiReturn<[SpriteBatch[], SpriteBatch[]]> => {
      if (res.piece_sprites[piecename]) {
        const sprites = res.piece_sprites[piecename];
        return $multi(sprites.pieces, sprites.ghosts);
      } else {
        res.piece_sprites[piecename] = {
          pieces: [],
          ghosts: [],
        };
        const sprites = res.piece_sprites[piecename];

        for (const rotation of rotations) {
          const sprite = love.graphics.newSpriteBatch(res.block_img);
          const ghostSprite = love.graphics.newSpriteBatch(res.block_img);
          const color = { r: 0, g: 0, b: 0 };
          [color.r, color.g, color.b] = Colors.get(colorname);
          sprite.setColor(color.r, color.g, color.b);
          ghostSprite.setColor(color.r, color.g, color.b, 0.2);
          for (const coord of rotation) {
            sprite.add(coord.x * board_grid_size, coord.y * board_grid_size);
            ghostSprite.add(coord.x * board_grid_size, coord.y * board_grid_size);
          }
          table.insert(sprites.pieces, sprite);
          table.insert(sprites.ghosts, ghostSprite);
        }
        return $multi(sprites.pieces, sprites.ghosts);
      }
    },
  };
}

export = Resources.res;
