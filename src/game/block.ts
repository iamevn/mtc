import type { SpriteBatch } from 'love.graphics';

import type { ColorName } from '../colors';
import * as res from '../res';
import { BaseObject } from '../types';
import type { Coordinate } from '../types';
import type { Board } from './board';

export class Block extends BaseObject {
  public posn: Coordinate;
  public color: ColorName;
  public board: Board;
  public spr: SpriteBatch;
  public flashing?: number;

  constructor(x: number, y: number, color: ColorName, board: Board) {
    super();
    this.posn = { x: x, y: y };
    this.color = color;
    this.board = board;
    this.spr = res.getBlockSprite(this.color);
    this.spr.add(0, 0);
  }

  draw(): void {
    const posn: Coordinate = {
      x: this.posn.x * this.board.grid.size,
      y: this.posn.y * this.board.grid.size,
    };

    love.graphics.push('all');

    if (this.flashing) {
      if (this.flashing > 8) {
        // whiten by drawing again with additive blend mode
        love.graphics.setBlendMode('add');
        love.graphics.draw(this.spr, posn.x, posn.y);
      } else {
        // fade out
        love.graphics.setColor(1, 1, 1, 0.3);
      }
    }

    love.graphics.draw(this.spr, posn.x, posn.y);
    love.graphics.pop();
  }

  tick(): void {
    if (this.flashing) {
      this.flashing -= 1;
      if (this.flashing == 0) {
        this.flashing = null;
      }
    }
  }

  flash(duration: number): void {
    this.flashing = duration || 12;
  }
}
