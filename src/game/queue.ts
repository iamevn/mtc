import { BaseObject } from '../types';
import { CounterLevel } from './attacks';
import type { Bag } from './bag';
import type { Board } from './board';
import type { clockPhase } from './clock';
import { Piece } from './piece';

export class Queue extends BaseObject {
  board: Board;
  bag: Bag;
  next_piece: Piece;
  attack_queue: Piece[];
  _attack_queue_size: number;

  constructor(board: Board, bag: Bag) {
    super();

    this.board = board;
    this.bag = bag;
    this.next_piece = this.bag.pull(this.board);
    this.attack_queue = [];
    this._attack_queue_size = 14;
  }

  pull(): Piece {
    const piece = this.next_piece;
    if (this.attack_queue.length > 0) {
      this.next_piece = table.remove(this.attack_queue, 1);
    } else {
      this.next_piece = this.bag.pull(this.board);
    }
    return piece;
  }

  addAttack(counter_level?: CounterLevel, clock_phase?: clockPhase): void {
    if (this.attack_queue.length == this._attack_queue_size) {
      this.overflowAttack();
    } else {
      let new_piece: Piece;
      if (counter_level) {
        new_piece = new Piece(this.board.grid.size, this.board, { counter_level: counter_level });
      } else {
        new_piece = new Piece(this.board.grid.size, this.board, { attack: true, clock_phase: clock_phase });
      }
      table.insert(this.attack_queue, new_piece);
    }
  }

  overflowAttack(): void {
    let i = this._attack_queue_size;
    while (i > 0 && this.attack_queue[i - 1].is_overflow) {
      i -= 1;
    }

    if (i > 0) {
      this.attack_queue[i - 1] = new Piece(this.board.grid.size, this.board, { overflow: true });
    }
  }

  can_counter(): boolean {
    return this.attack_queue.length > 0;
  }

  counter(): CounterLevel | null {
    if (!this.can_counter()) {
      return null;
    }

    const last_piece = this.attack_queue[this.attack_queue.length - 1];
    let new_counter: CounterLevel = 1;
    if (last_piece.is_counter) {
      if (last_piece.counter_level == 4) {
        new_counter = 4;
      } else {
        new_counter = (last_piece.counter_level + 1) as CounterLevel;
      }
    }
    table.remove(this.attack_queue, this.attack_queue.length);
    return new_counter;
  }

  draw(): void {
    // draw attack queue very small and along top of board
    const spacing = 20;
    for (const [i, piece] of ipairs(this.attack_queue)) {
      let x: number;
      if (i <= this._attack_queue_size / 2) {
        x = i * spacing;
      } else {
        x = (i - this._attack_queue_size / 2) * spacing;
      }
      x -= spacing;

      let y: number;
      if (i > this._attack_queue_size / 2) {
        y = spacing;
      } else {
        y = 0;
      }

      love.graphics.push();
      love.graphics.translate(x, y);
      love.graphics.scale(0.3);
      piece.draw(true, true);
      love.graphics.pop();
    }

    // draw next piece smaller and at top of board
    love.graphics.push();
    love.graphics.translate(this.board.grid.width * this.board.grid.size, 0);
    love.graphics.scale(0.8);
    this.next_piece.draw(true);
    love.graphics.pop();
  }
}
