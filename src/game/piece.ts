import type { Image, SpriteBatch } from 'love.graphics';

import type { ColorName } from '../colors';
import * as res from '../res';
import { BaseObject } from '../types';
import type { Coordinate } from '../types';
import type { CounterLevel } from './attacks';
import type { Board } from './board';
import type { clockPhase } from './clock';
import { kickClockwise, kickCounterclockwise } from './kick';
import { buildCounter, buildOverflow, buildPhasedAttack, build as buildTetromino } from './tetromino';

export class Piece extends BaseObject {
  public board: Board;
  public posn: Coordinate;

  public name: string;
  public color: ColorName;
  public rotations: Coordinate[][];
  public sprites: SpriteBatch[];
  public ghostSprites: SpriteBatch[];
  public rotation_idx = 1;

  public moving_left = false;
  public moving_right = false;

  public is_counter = false;
  public counter_level?: CounterLevel;
  public is_attack = false;
  public is_overflow = false;

  gridsize: number;

  constructor(
    gridsize: number,
    board?: Board,
    options?: {
      counter_level?: CounterLevel;
      attack?: boolean;
      overflow?: boolean;
      piece?: string;
      clock_phase?: clockPhase;
    }
  ) {
    super();
    this.gridsize = gridsize;
    this.board = board;
    this.posn = { x: 4, y: 2 }; // TODO: maybe a different starting height
    this.moving_left = false;
    this.moving_right = false;

    if (options && options.counter_level) {
      [this.name, this.color, this.rotations] = buildCounter(options.counter_level);
      this.is_counter = true;
      this.counter_level = options.counter_level;
    } else if (options && options.attack) {
      [this.name, this.color, this.rotations] = buildPhasedAttack(options.clock_phase);
      this.is_attack = true;
    } else if (options && options.overflow) {
      [this.name, this.color, this.rotations] = buildOverflow();
      this.is_overflow = true;
    } else if (options && options.piece) {
      [this.name, this.color, this.rotations] = buildTetromino(options.piece);
    } else {
      [this.name, this.color, this.rotations] = buildTetromino();
    }

    [this.sprites, this.ghostSprites] = res.getPieceSprites(this.name, this.color, this.rotations, gridsize);
    this.rotation_idx = 1;
  }

  private initSprites(gridsize: number): void {
    if (gridsize != this.gridsize) {
      this.gridsize = gridsize;
      [this.sprites, this.ghostSprites] = res.getPieceSprites(this.name, this.color, this.rotations, gridsize);
    }
  }

  public setBoard(board: Board): void {
    this.board = board;
    this.initSprites(this.board.grid.size);
  }

  public ghostYCoord(): number {
    let dy = 0;

    while (this.canMove(0, dy + 1)) {
      dy += 1;
    }
    return this.posn.y + dy;
  }

  public draw(only_piece?: boolean, in_queue?: boolean): void {
    if (only_piece) {
      let sprite: Image | SpriteBatch = this.sprites[this.rotation_idx - 1];
      if (in_queue) {
        if (this.is_counter) {
          sprite = res.counter_img;
        } else if (this.is_overflow) {
          sprite = res.overflow_img;
        }
      }
      love.graphics.draw(sprite);
    } else {
      const x = this.posn.x * this.gridsize;
      const y = this.posn.y * this.gridsize;
      const ghost_y = this.ghostYCoord() * this.gridsize;

      love.graphics.draw(this.ghostSprites[this.rotation_idx - 1], x, ghost_y);
      love.graphics.draw(this.sprites[this.rotation_idx - 1], x, y);
    }
  }

  // TODO: make this an iterator
  public squareCoords(): Coordinate[] {
    const coords: Coordinate[] = [];
    for (const coord of this.rotations[this.rotation_idx - 1]) {
      table.insert(coords, { x: this.posn.x + coord.x, y: this.posn.y + coord.y });
    }
    return coords;
  }

  public canMove(dx: number, dy: number): boolean {
    if (!this.board) {
      return false;
    }
    for (const coord of this.rotations[this.rotation_idx - 1]) {
      const x = this.posn.x + coord.x + dx;
      const y = this.posn.y + coord.y + dy;
      if (!this.board.open(x, y)) {
        return false;
      }
    }
    return true;
  }

  public move(dx: number, dy: number): void {
    this.posn.x += dx;
    this.posn.y += dy;
  }

  public canRotateClockwise(): boolean {
    if (!this.board) {
      return false;
    }
    let rotated_idx = this.rotation_idx + 1;
    if (rotated_idx > this.rotations.length) {
      rotated_idx = 1;
    }

    const rotated_squares = this.rotations[rotated_idx - 1];
    for (const coord of rotated_squares) {
      const x = this.posn.x + coord.x;
      const y = this.posn.y + coord.y;
      if (!this.board.open(x, y)) {
        return false;
      }
    }

    return true;
  }

  public canRotateCounterClockwise(): boolean {
    if (!this.board) {
      return false;
    }
    let rotated_idx = this.rotation_idx - 1;
    if (rotated_idx == 0) {
      rotated_idx = this.rotations.length;
    }

    const rotated_squares = this.rotations[rotated_idx - 1];
    for (const coord of rotated_squares) {
      const x = this.posn.x + coord.x;
      const y = this.posn.y + coord.y;
      if (!this.board.open(x, y)) {
        return false;
      }
    }

    return true;
  }

  public rotateClockwise(): boolean {
    if (!this.board) {
      return false;
    }
    let new_idx = this.rotation_idx + 1;
    if (new_idx > this.rotations.length) {
      new_idx = 1;
    }

    if (this.canRotateClockwise()) {
      this.rotation_idx = new_idx;
      return true;
    } else {
      const kicker = kickClockwise(this.rotations[new_idx - 1], this.board, this.posn);
      const old_idx = this.rotation_idx;
      this.rotation_idx = new_idx;
      if (kicker == 'left' && this.canMove(-1, 0)) {
        this.move(-1, 0);
        return true;
      } else if (kicker == 'right' && this.canMove(1, 0)) {
        this.move(1, 0);
        return true;
      } else if (kicker == 'down' && this.canMove(0, 1)) {
        this.move(0, 1);
        return true;
      } else if (kicker == 'up' && this.canMove(0, -1)) {
        this.move(0, -1);
        return true;
      } else if (kicker == '2 down' && this.canMove(0, 2)) {
        this.move(0, 2);
        return true;
      } else if (kicker == '2 up' && this.canMove(0, -2)) {
        this.move(0, -2);
        return true;
      } else if (kicker == '2 right' && this.canMove(2, 0)) {
        this.move(2, 0);
        return true;
      } else if (kicker == '2 left' && this.canMove(-2, 0)) {
        this.move(-2, 0);
        return true;
      } else {
        this.rotation_idx = old_idx;
        return false;
      }
    }
  }

  public rotateCounterClockwise(): boolean {
    if (!this.board) {
      return false;
    }
    let new_idx = this.rotation_idx - 1;
    if (new_idx == 0) {
      new_idx = this.rotations.length;
    }

    if (this.canRotateCounterClockwise()) {
      this.rotation_idx = new_idx;
      return true;
    } else {
      const kicker = kickCounterclockwise(this.rotations[new_idx - 1], this.board, this.posn);
      const old_idx = this.rotation_idx;
      this.rotation_idx = new_idx;
      if (kicker == 'left' && this.canMove(-1, 0)) {
        this.move(-1, 0);
        return true;
      } else if (kicker == 'right' && this.canMove(1, 0)) {
        this.move(1, 0);
        return true;
      } else if (kicker == 'down' && this.canMove(0, 1)) {
        this.move(0, 1);
        return true;
      } else if (kicker == 'up' && this.canMove(0, -1)) {
        this.move(0, -1);
        return true;
      } else if (kicker == '2 down' && this.canMove(0, 2)) {
        this.move(0, 2);
        return true;
      } else if (kicker == '2 up' && this.canMove(0, -2)) {
        this.move(0, -2);
        return true;
      } else if (kicker == '2 right' && this.canMove(2, 0)) {
        this.move(2, 0);
        return true;
      } else if (kicker == '2 left' && this.canMove(-2, 0)) {
        this.move(-2, 0);
        return true;
      } else {
        this.rotation_idx = old_idx;
        return false;
      }
    }
  }
}
