import { BaseObject } from '../types';

export type clockPhase = 1 | 2 | 3 | 4;
const max_phase = 4;

export class Clock extends BaseObject {
  phase: clockPhase = 1;
  time_per_phase = 30;
  private time_since_phase = 0;

  update(dt): void {
    this.time_since_phase += dt;
    if (this.time_since_phase >= this.time_per_phase) {
      this.toNextPhase();
    }
  }
  private toNextPhase(): void {
    const next_phase = math.min(max_phase, this.phase + 1) as clockPhase;
    this.changePhase(next_phase);
  }

  private changePhase(n: clockPhase) {
    this.phase = n;
    this.time_since_phase -= this.time_per_phase;
  }

  draw(): void {
    const level = `LV ${this.phase}/${max_phase}`;
    const timeleft = string.format('%.0d', this.time_per_phase - this.time_since_phase);
    let s: string;
    if (this.phase < max_phase) {
      s = [level, timeleft].join(' ');
    } else {
      s = level;
    }

    love.graphics.printf(s, 0, 0, 50, 'center');
  }
}
