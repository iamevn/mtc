import { BaseObject } from '../types';
import type { Board } from './board';
import { Piece } from './piece';

export class Bag extends BaseObject {
  nextPiece: Piece;
  grid_size: number;
  i_count = 0;

  constructor(grid_size: number) {
    super();
    this.grid_size = grid_size;
    this.nextPiece = new Piece(this.grid_size);
  }

  draw(): void {
    love.graphics.push('all');
    love.graphics.print('next piece:');
    love.graphics.translate(-40, 5);
    this.nextPiece.draw();
    love.graphics.pop();
  }

  pull(board: Board): Piece {
    const piece = this.nextPiece;
    if (this.i_count > 0) {
      this.nextPiece = new Piece(this.grid_size, null, { piece: 'I' });
      this.i_count -= 1;
    } else {
      this.nextPiece = new Piece(this.grid_size);
    }
    piece.setBoard(board);
    return piece;
  }

  queue_I(): void {
    // TODO: check if it should be set to 6 or increase by 6
    this.i_count += 6;
  }
}
