import type { Coordinate } from '../types';
import type { Board } from './board';

type KickTags = 'red' | 'orange' | 'yellow' | 'green' | 'cyan' | 'grey' | 'blue' | 'purple';
type Direction = string;

interface Colordict {
  red?: Direction;
  orange?: Direction;
  yellow?: Direction;
  green?: Direction;
  cyan?: Direction;
  grey?: Direction;
  blue?: Direction;
  purple?: Direction;
}

type KickSquare = {
  x: number;
  y: number;
  dir: Direction;
};

type Kick = {
  tag: KickTags;
  squares: KickSquare[];
};

type KickMap = Kick[];

const ccw_kicks: KickMap = [
  {
    tag: 'red',
    squares: [
      { x: 1, y: -2, dir: 'down' },
      { x: 2, y: -1, dir: 'left' },
      { x: -1, y: 2, dir: 'up' },
      { x: -2, y: -1, dir: 'right' },
    ],
  },
  {
    tag: 'orange',
    squares: [{ x: 1, y: 0, dir: 'left' }],
  },
  {
    tag: 'yellow',
    squares: [{ x: -1, y: 0, dir: 'right' }],
  },
  {
    tag: 'green',
    squares: [
      { x: 2, y: -1, dir: 'left' },
      { x: 1, y: 2, dir: 'up' },
      { x: -2, y: 1, dir: 'right' },
      { x: -1, y: -2, dir: 'down' },
    ],
  },
  {
    tag: 'cyan',
    squares: [
      { x: 0, y: -1, dir: 'down' },
      { x: 0, y: 1, dir: 'up' },
    ],
  },
  {
    tag: 'grey',
    squares: [
      { x: 0, y: -2, dir: 'down' },
      { x: 0, y: 2, dir: 'up' },
      { x: -2, y: 0, dir: 'right' },
      { x: 2, y: 0, dir: 'left' },
    ],
  },
  {
    tag: 'blue',
    squares: [
      { x: -2, y: 2, dir: 'left' },
      { x: -1, y: 1, dir: 'left' },
      { x: 1, y: 1, dir: 'left' },
      { x: 2, y: 2, dir: 'left' },
    ],
  },
  {
    tag: 'purple',
    squares: [
      { x: -2, y: -2, dir: 'right' },
      { x: -1, y: -1, dir: 'right' },
      { x: 1, y: -1, dir: 'right' },
      { x: 2, y: -2, dir: 'right' },
    ],
  },
];

const cw_kicks: KickMap = [
  {
    tag: 'red',
    squares: [
      { x: 2, y: -1, dir: 'left' },
      { x: 1, y: 2, dir: 'up' },
      { x: -2, y: 1, dir: 'right' },
      { x: -1, y: -2, dir: 'down' },
    ],
  },
  {
    tag: 'orange',
    squares: [{ x: -1, y: 0, dir: 'right' }],
  },
  {
    tag: 'yellow',
    squares: [{ x: 1, y: 0, dir: 'left' }],
  },
  {
    tag: 'green',
    squares: [
      { x: 1, y: -2, dir: 'down' },
      { x: 2, y: -1, dir: 'left' },
      { x: -1, y: 2, dir: 'up' },
      { x: -2, y: -1, dir: 'right' },
    ],
  },
  {
    tag: 'cyan',
    squares: [
      { x: 0, y: -1, dir: 'down' },
      { x: 0, y: 1, dir: 'up' },
    ],
  },
  {
    tag: 'grey',
    squares: [
      { x: 0, y: -2, dir: 'down' },
      { x: 0, y: 2, dir: 'up' },
      { x: -2, y: 0, dir: 'right' },
      { x: 2, y: 0, dir: 'left' },
    ],
  },
  {
    tag: 'blue',
    squares: [
      { x: -2, y: 2, dir: 'right' },
      { x: -1, y: 1, dir: 'right' },
      { x: 1, y: 1, dir: 'right' },
      { x: 2, y: 2, dir: 'right' },
    ],
  },
  {
    tag: 'purple',
    squares: [
      { x: -2, y: -2, dir: 'left' },
      { x: -1, y: -1, dir: 'left' },
      { x: 1, y: -1, dir: 'left' },
      { x: 2, y: -2, dir: 'left' },
    ],
  },
];

type KickResult = string | false;

function kickDir(piece_squares: Coordinate[], board: Board, offset: Coordinate, way: KickMap): KickResult {
  const kicks: Colordict = {};
  for (const kickmap of way) {
    const color = kickmap.tag;
    for (const kick of kickmap.squares) {
      for (const piece_square of piece_squares) {
        if (piece_square.x == kick.x && piece_square.y == kick.y) {
          if (!board.open(kick.x + offset.x, kick.y + offset.y)) {
            if (!kicks[color]) {
              kicks[color] = kick.dir;
            } else if (kicks[color] != kick.dir) {
              kicks[color] = null;
              // unsure if should break,
              // pretty sure the edge case where it'd matter will never come up with these pieces
              // e.g. the big L piece that covers 3 blue squares can't rotate so that it'd get
              // kicked left left right.
              // only one that should come up is double cyan which won't matter whether it breaks
              // break
            }
          }
        }
      }
    }
  }

  if (kicks.red) {
    return kicks.red;
  } else if (kicks.orange) {
    if (kicks.grey == kicks.orange) {
      return '2 ' + kicks.orange;
    } else {
      return kicks.orange;
    }
  } else if (kicks.yellow) {
    if (kicks.grey == kicks.yellow) {
      return '2 ' + kicks.yellow;
    } else {
      return kicks.yellow;
    }
  } else if (kicks.green) {
    return kicks.green;
  } else if (kicks.cyan) {
    if (kicks.grey == kicks.cyan) {
      return '2 ' + kicks.cyan;
    } else {
      return kicks.cyan;
    }
  } else if (kicks.grey) {
    return kicks.grey;
  } else if (kicks.blue) {
    return kicks.blue;
  } else if (kicks.purple) {
    return kicks.purple;
  } else {
    return false;
  }
}

export const kickClockwise = (piece_squares: Coordinate[], board: Board, offset: Coordinate): KickResult =>
  kickDir(piece_squares, board, offset, cw_kicks);
export const kickCounterclockwise = (piece_squares: Coordinate[], board: Board, offset: Coordinate): KickResult =>
  kickDir(piece_squares, board, offset, ccw_kicks);
