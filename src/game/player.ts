import { BaseObject } from '../types';
import type { Input } from '../types';
import { CounterLevel } from './attacks';
import type { Bag } from './bag';
import { Board } from './board';
import { Clock } from './clock';
import { MagicMeter } from './magicmeter';
import type { Piece } from './piece';
import { GameOver } from './player_state/gameover';
import type { PlayerState } from './player_state/playerstate';
import { Playing } from './player_state/playing';
import { Prespawn } from './player_state/prespawn';

const constants = {
  gravity_by_phase: { 1: 60, 2: 40, 3: 30, 4: 20 }, // TODO: check game for timing
  DAS_delay: 9,
  DAS_repeat: 1,
  lock_delay: 30,
  ARE: 31,
  base_line_clear_delay: 16,
  precharge_das: true,
};

export class Player extends BaseObject {
  input: Input;
  bag: Bag;
  clock: Clock;
  ticks_until: {
    spawn?: number;
    gravity?: number;
    rightshift?: number;
    leftshift?: number;
    lock?: number;
  };
  queued_moves: {
    hard_drop?: boolean;
    spin?: 'clockwise' | 'counterclockwise';
    right?: boolean;
    left?: boolean;
    down?: boolean;
  };
  held_moves: {
    right?: number;
    left?: number;
    down?: boolean;
  };
  score: {
    points: number;
    combo: number;
  };
  board: Board;
  magicmeter: MagicMeter;
  piece: Piece;
  state: PlayerState;
  states: {
    Prespawn: Prespawn;
    GameOver: GameOver;
    Playing: Playing;
  };
  constants: typeof constants;

  // opponent player, TODO: actually hook this up
  opponent?: Player;

  constructor(input: Input, bag: Bag, clock: Clock) {
    super();
    this.input = input;
    this.bag = bag;
    this.clock = clock;
    this.ticks_until = {};
    this.queued_moves = {};
    this.held_moves = {};
    this.score = {
      points: 0,
      combo: 0,
    };
    this.constants = constants;

    this.board = new Board(this.bag);
    this.magicmeter = new MagicMeter(this.board);

    this.ticks_until.spawn = constants.ARE;

    this.states = {
      Prespawn: new Prespawn(this),
      GameOver: new GameOver(this),
      Playing: new Playing(this),
    };

    this.changeState(this.states.Prespawn);
  }

  tick(): void {
    this.state.tick();
    this.board.tick();
  }

  update(dt: number): void {
    this.input.update();
    if (this.input.pressed('debug_1')) {
      print('adding attack piece to queue');
      this.receiveAttack();
      /*
      constants.precharge_das = !constants.precharge_das;
      if (constants.precharge_das) {
        print('DAS precharge enabled');
      } else {
        print('DAS precharge disabled');
      }
      */
    }
    if (this.input.pressed('debug_2')) {
      print('adding counter 1 piece to queue');
      this.receiveAttack(1);
    }
    if (this.input.pressed('debug_3')) {
      print('adding counter 2 piece to queue');
      this.receiveAttack(2);
    }
    if (this.input.pressed('debug_4')) {
      print('adding counter 3 piece to queue');
      this.receiveAttack(3);
    }
    if (this.input.pressed('debug_5')) {
      print('adding counter 4 piece to queue');
      this.receiveAttack(4);
    }

    this.state.checkInput();
    this.board.update(dt);
  }

  lockPiece(): void {
    this.board.lock(this.piece);
    const toClear = this.board.checkClears();

    for (const row of toClear) {
      this.board.scheduleClear(row);
    }

    if (toClear.length > 0) {
      this.scoreLines(toClear.length);
      this.attack(toClear.length as 1 | 2 | 3 | 4 | 5, this.score.combo, 1);
      this.ticks_until.spawn = constants.base_line_clear_delay + toClear.length;
    } else {
      this.resetCombo();
      this.ticks_until.spawn = constants.ARE;
    }

    this.changeState(this.states.Prespawn);
  }

  scoreLines(lineCount: number): void {
    const scorings = {
      1: 100,
      2: 250,
      3: 750,
      4: 3000,
      5: 5000,
    };

    const value = scorings[lineCount] * math.pow(2, this.score.combo);

    this.score.combo = this.score.combo + 1;
    this.score.points = this.score.points + value;
  }

  resetCombo(): void {
    this.score.combo = 0;
  }

  spawnPiece(): boolean {
    this.piece = this.board.queue.pull();
    this.ticks_until.gravity = this.current_gravity();

    for (const coord of this.piece.squareCoords()) {
      if (this.board.occupied(coord.x, coord.y)) {
        return false;
      }
    }

    return true;
  }

  changeState(newState: PlayerState): void {
    if (this.state) {
      this.state.onExit();
    }

    this.state = newState;

    this.state.onEnter();
  }

  draw(): void {
    this.state.draw();
  }
  drawScore(): void {
    love.graphics.print(string.format('Score: %06d', this.score.points), 10, -20);
  }

  drawCombo(): void {
    const board_width = this.board.grid.px_width;
    const board_height = this.board.grid.px_height;
    const s = string.format('%d combo!', this.score.combo);
    if (this.score.combo > 1) {
      love.graphics.printf(s, 0, board_height / 3, board_width, 'center');
    }
  }

  receiveAttack(counter_level?: CounterLevel): void {
    this.board.queue.addAttack(counter_level, this.clock.phase);
  }

  sendAttack(counter_level?: CounterLevel): void {
    if (this.opponent) {
      this.opponent.receiveAttack(counter_level);
    }
  }

  attack(lines: 1 | 2 | 3 | 4 | 5, combo: number, intensity: 1 | 2 | 3 | 4 = 1): void {
    if (combo >= 2) {
      for (const _ of $range(2, combo)) {
        this.sendAttack(this.board.queue.counter());
      }
    }

    let x = {
      1: { 1: 0, 2: 1, 3: 1, 4: 2 },
      2: { 1: 3, 2: 3, 3: 4, 4: 4 },
      3: { 1: 4, 2: 4, 3: 5, 4: 6 },
      4: { 1: 6, 2: 6, 3: 7, 4: 8 },
      5: { 1: 10, 2: 10, 3: 10, 4: 10 },
      // lines=5, intensity=1 needs checking but is unlikely to come up during intensity 1
    }[lines][intensity];
    while (x > 0) {
      this.sendAttack(this.board.queue.counter());
      x--;
    }

    let y = 0;
    if (lines == 1) {
      y = { 1: 1, 2: 2, 3: 2, 4: 2 }[intensity];
    }
    while (y > 0) {
      if (this.board.queue.can_counter()) {
        this.sendAttack(this.board.queue.counter());
      } else {
        // not 100% sure this is right since it may add up to 2 charges in intensity 2, 3, and 4
        // but it's what http://tasvideos.org/GameResources/N64/MagicalTetrisChallenge.html says
        this.magicmeter.addCharge();
      }
      y--;
    }
  }

  current_gravity(): number {
    return this.constants.gravity_by_phase[this.clock.phase];
  }
}
