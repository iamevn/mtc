import type { ColorName } from '../colors';
import type { StringDictionary } from '../types';

export interface Tetromino {
  name: string;
  color: ColorName;
  layout: boolean[][];
}

const _ = false;
const o = true;

export const normalTetrominos: Tetromino[] = [
  {
    name: 'I',
    color: 'red',
    // prettier-ignore
    layout: [ 
      [ 
        _, _, _, _, _,
        _, _, o, _, _,
        _, _, o, _, _,
        _, _, o, _, _,
        _, _, o, _, _,
       ],
      [ 
        _, _, _, _, _,
        _, _, _, _, _,
        _, o, o, o, o,
        _, _, _, _, _,
        _, _, _, _, _,
       ],
     ],
  },
  {
    name: 'L',
    color: 'orange',
    // prettier-ignore
    layout: [ 
      [ 
        _, _, _, _, _,
        _, _, o, o, _,
        _, _, _, o, _,
        _, _, _, o, _,
        _, _, _, _, _,
       ],
      [ 
        _, _, _, _, _,
        _, _, _, _, _,
        _, _, _, o, _,
        _, o, o, o, _,
        _, _, _, _, _,
       ],
      [ 
        _, _, _, _, _,
        _, o, _, _, _,
        _, o, _, _, _,
        _, o, o, _, _,
        _, _, _, _, _,
       ],
      [ 
        _, _, _, _, _,
        _, o, o, o, _,
        _, o, _, _, _,
        _, _, _, _, _,
        _, _, _, _, _,
       ],
     ],
  },
  {
    name: 'J',
    color: 'blue',
    // prettier-ignore
    layout: [ 
      [ 
        _, _, _, _, _,
        _, o, o, _, _,
        _, o, _, _, _,
        _, o, _, _, _,
        _, _, _, _, _,
       ],
      [ 
        _, _, _, _, _,
        _, o, o, o, _,
        _, _, _, o, _,
        _, _, _, _, _,
        _, _, _, _, _,
       ],
      [ 
        _, _, _, _, _,
        _, _, _, o, _,
        _, _, _, o, _,
        _, _, o, o, _,
        _, _, _, _, _,
       ],
      [ 
        _, _, _, _, _,
        _, _, _, _, _,
        _, o, _, _, _,
        _, o, o, o, _,
        _, _, _, _, _,
       ],
     ],
  },
  {
    name: 'T',
    color: 'cyan',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, _, _, _, _,
        _, o, o, o, _,
        _, _, o, _, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, o, _, _,
        _, o, o, _, _,
        _, _, o, _, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, o, _, _,
        _, o, o, o, _,
        _, _, _, _, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, o, _, _,
        _, _, o, o, _,
        _, _, o, _, _,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'S',
    color: 'magenta',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, o, _, _, _,
        _, o, o, _, _,
        _, _, o, _, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, o, o, _,
        _, o, o, _, _,
        _, _, _, _, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, o, _, _,
        _, _, o, o, _,
        _, _, _, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, _, _, _,
        _, _, o, o, _,
        _, o, o, _, _,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'Z',
    color: 'green',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, _, _, o, _,
        _, _, o, o, _,
        _, _, o, _, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, _, _, _,
        _, o, o, _, _,
        _, _, o, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, o, _, _,
        _, o, o, _, _,
        _, o, _, _, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, o, _, _,
        _, _, o, o, _,
        _, _, _, _, _,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'O',
    color: 'yellow',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, _, _, _, _,
        _, _, o, o, _,
        _, _, o, o, _,
        _, _, _, _, _,
      ],
    ],
  },
];

export const attackTetrominos: Tetromino[] = [
  {
    name: 'big_I',
    color: 'white',
    // prettier-ignore
    layout: [
      [
        _, _, o, _, _,
        _, _, o, _, _,
        _, _, o, _, _,
        _, _, o, _, _,
        _, _, o, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, _, _, _,
        o, o, o, o, o,
        _, _, _, _, _,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'd',
    color: 'green',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, _, _, o, _,
        _, _, o, o, _,
        _, _, o, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, _, _, _,
        _, _, o, o, _,
        _, _, o, o, o,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, _, _, _,
        _, _, o, o, _,
        _, _, o, o, _,
        _, _, o, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, _, _, _,
        _, o, o, o, _,
        _, _, o, o, _,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'b',
    color: 'magenta',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, _, o, _, _,
        _, _, o, o, _,
        _, _, o, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, _, _, _,
        _, _, o, o, o,
        _, _, o, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, _, _, _,
        _, _, o, o, _,
        _, _, o, o, _,
        _, _, _, o, _,
      ],
      [
        _, _, _, _, _,
        _, _, _, _, _,
        _, _, o, o, _,
        _, o, o, o, _,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'u',
    color: 'peach',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, o, _, o, _,
        _, o, o, o, _,
        _, _, _, _, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, o, o, _,
        _, _, o, _, _,
        _, _, o, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, _, _, _,
        _, o, o, o, _,
        _, o, _, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, o, _, _,
        _, _, o, _, _,
        _, o, o, _, _,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'ugh',
    color: 'orange',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, _, _, o, _,
        _, o, o, o, _,
        _, _, o, _, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, o, _, _,
        _, o, o, _, _,
        _, _, o, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, o, _, _,
        _, o, o, o, _,
        _, o, _, _, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, o, _, _,
        _, _, o, o, _,
        _, _, o, _, _,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'hug',
    color: 'blue',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, o, _, _, _,
        _, o, o, o, _,
        _, _, o, _, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, o, o, _,
        _, o, o, _, _,
        _, _, o, _, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, o, _, _,
        _, o, o, o, _,
        _, _, _, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, o, _, _,
        _, _, o, o, _,
        _, o, o, _, _,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'stairs',
    color: 'teal',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, _, _, o, _,
        _, _, o, o, _,
        _, o, o, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, _, _, _,
        _, o, o, _, _,
        _, o, o, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, o, o, _,
        _, o, o, _, _,
        _, o, _, _, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, o, o, _,
        _, _, o, o, _,
        _, _, _, o, _,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'corner',
    color: 'purple',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, _, _, o, _,
        _, _, _, o, _,
        _, o, o, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, _, _, _,
        _, o, _, _, _,
        _, o, o, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, o, o, _,
        _, o, _, _, _,
        _, o, _, _, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, o, o, _,
        _, _, _, o, _,
        _, _, _, o, _,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'zigzag',
    color: 'teal',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, _, _, o, _,
        _, _, o, o, _,
        _, o, o, _, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, _, _, _,
        _, o, o, _, _,
        _, _, o, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, o, o, _,
        _, o, o, _, _,
        _, o, _, _, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, o, _, _,
        _, _, o, o, _,
        _, _, _, o, _,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'big_s',
    color: 'magenta',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, o, o, _, _,
        _, _, o, _, _,
        _, _, o, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, _, o, _,
        _, o, o, o, _,
        _, o, _, _, _,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'big_z',
    color: 'green',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, _, o, o, _,
        _, _, o, _, _,
        _, o, o, _, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, _, _, _,
        _, o, o, o, _,
        _, _, _, o, _,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'plus',
    color: 'purple',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, _, o, _, _,
        _, o, o, o, _,
        _, _, o, _, _,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'amogus',
    color: 'peach',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, o, _, o, _,
        _, o, o, o, _,
        _, o, o, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, o, o, _,
        _, o, o, _, _,
        _, o, o, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, o, o, _,
        _, o, o, o, _,
        _, o, _, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, o, o, _,
        _, _, o, o, _,
        _, o, o, o, _,
        _, _, _, _, _,
      ],
    ],
  },
];

export const counterTetrominos: Tetromino[] = [
  {
    name: 'small_counter',
    color: 'green',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, _, _, _, _,
        _, _, o, o, _,
        _, _, o, o, _,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'medium_counter',
    color: 'blue',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, o, o, o, _,
        _, o, o, o, _,
        _, o, o, o, _,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'large_counter',
    color: 'red',
    // prettier-ignore
    layout: [
      [
        _, o, o, o, o,
        _, o, o, o, o,
        _, o, o, o, o,
        _, o, o, o, o,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'giant_counter',
    color: 'red',
    // prettier-ignore
    layout: [
      [
        o, o, o, o, o,
        o, o, o, o, o,
        o, o, o, o, o,
        o, o, o, o, o,
        o, o, o, o, o,
      ],
    ],
  },
];

export const overflowTetrominos: Tetromino[] = [
  {
    name: 'big_stairs',
    color: 'teal',
    // prettier-ignore
    layout: [
      [
        o, o, o, o, o,
        o, o, o, o, _,
        o, o, o, _, _,
        o, o, _, _, _,
        o, _, _, _, _,
      ],
      [
        o, o, o, o, o,
        _, o, o, o, o,
        _, _, o, o, o,
        _, _, _, o, o,
        _, _, _, _, o,
      ],
      [
        _, _, _, _, o,
        _, _, _, o, o,
        _, _, o, o, o,
        _, o, o, o, o,
        o, o, o, o, o,
      ],
      [
        o, _, _, _, _,
        o, o, _, _, _,
        o, o, o, _, _,
        o, o, o, o, _,
        o, o, o, o, o,
      ],
    ],
  },
  {
    name: 'big_diamond',
    color: 'purple',
    // prettier-ignore
    layout: [
      [
        _, _, o, _, _,
        _, o, o, o, _,
        o, o, o, o, o,
        _, o, o, o, _,
        _, _, o, _, _,
      ],
    ],
  },
  {
    name: 'big_triangle',
    color: 'peach',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        o, o, o, o, o,
        _, o, o, o, _,
        _, _, o, _, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, o, _,
        _, _, o, o, _,
        _, o, o, o, _,
        _, _, o, o, _,
        _, _, _, o, _,
      ],
      [
        _, _, _, _, _,
        _, _, o, _, _,
        _, o, o, o, _,
        o, o, o, o, o,
        _, _, _, _, _,
      ],
      [
        _, o, _, _, _,
        _, o, o, _, _,
        _, o, o, o, _,
        _, o, o, _, _,
        _, o, _, _, _,
      ],
    ],
  },
];

export const forbiddenTetrominos: Tetromino[] = [
  {
    name: 'small i',
    color: 'blue',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, _, _, _, _,
        _, o, o, o, _,
        _, _, _, _, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, o, _, _,
        _, _, o, _, _,
        _, _, o, _, _,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'tall j',
    color: 'blue',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, _, o, _, _,
        _, _, o, _, _,
        _, _, o, _, _,
        _, o, o, _, _,
      ],
      [
        _, _, _, _, _,
        o, _, _, _, _,
        o, o, o, o, _,
        _, _, _, _, _,
        _, _, _, _, _,
      ],
      [
        _, _, o, o, _,
        _, _, o, _, _,
        _, _, o, _, _,
        _, _, o, _, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, _, _, _,
        _, o, o, o, o,
        _, _, _, _, o,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'tall l',
    color: 'orange',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, _, o, _, _,
        _, _, o, _, _,
        _, _, o, _, _,
        _, _, o, o, _,
      ],
      [
        _, _, _, _, _,
        _, _, _, _, _,
        o, o, o, o, _,
        o, _, _, _, _,
        _, _, _, _, _,
      ],
      [
        _, o, o, _, _,
        _, _, o, _, _,
        _, _, o, _, _,
        _, _, o, _, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, _, _, o,
        _, o, o, o, o,
        _, _, _, _, _,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'small corner',
    color: 'purple',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, _, _, _, _,
        _, _, _, o, _,
        _, _, o, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, _, _, _,
        _, _, o, _, _,
        _, _, o, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, _, _, _,
        _, _, o, o, _,
        _, _, o, _, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, _, _, _,
        _, _, o, o, _,
        _, _, _, o, _,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'borger',
    color: 'blue',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, o, o, o, _,
        _, _, _, _, _,
        _, o, o, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, _, o, _,
        _, o, _, o, _,
        _, o, _, o, _,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'Y',
    color: 'purple',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, _, o, _, _,
        _, o, o, o, _,
        _, o, _, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, o, _, _,
        _, _, o, o, _,
        _, o, o, _, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, _, o, _,
        _, o, o, o, _,
        _, _, o, _, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, o, o, _,
        _, o, o, _, _,
        _, _, o, o, _,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'H',
    color: 'peach',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, o, _, o, _,
        _, o, o, o, _,
        _, o, _, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, o, o, _,
        _, _, o, _, _,
        _, o, o, o, _,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'U',
    color: 'peach',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, o, _, o, _,
        _, o, _, o, _,
        _, o, o, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, o, o, _,
        _, o, _, _, _,
        _, o, o, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, o, o, _,
        _, o, _, o, _,
        _, o, _, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, o, o, _,
        _, _, _, o, _,
        _, o, o, o, _,
        _, _, _, _, _,
      ],
      ,
    ],
  },
  {
    name: 'hook j',
    color: 'blue',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, _, _, o, _,
        _, o, _, o, _,
        _, o, o, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, o, _, _,
        _, o, _, _, _,
        _, o, o, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, o, o, _,
        _, o, _, o, _,
        _, o, _, _, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, o, o, _,
        _, _, _, o, _,
        _, _, o, o, _,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'hook l',
    color: 'orange',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, o, _, _, _,
        _, o, _, o, _,
        _, o, o, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, o, o, _,
        _, o, _, _, _,
        _, o, o, _, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, o, o, _,
        _, o, _, o, _,
        _, _, _, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, o, o, _,
        _, _, _, o, _,
        _, o, o, o, _,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'tank',
    color: 'yellow',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, o, o, o, _,
        _, o, o, o, _,
        _, _, o, _, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, o, o, _,
        _, o, o, o, _,
        _, _, o, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, o, _, _,
        _, o, o, o, _,
        _, o, o, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, o, _, _,
        _, o, o, o, _,
        _, o, o, _, _,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'orb',
    color: 'yellow',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, _, o, o, _,
        _, o, o, o, o,
        _, _, o, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, _, o, _, _,
        _, o, o, o, _,
        _, o, o, o, _,
        _, _, o, _, _,
      ],
    ],
  },
  {
    name: 'tall h',
    color: 'peach',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, o, _, o, _,
        _, o, o, o, _,
        _, o, o, o, _,
        _, o, _, o, _,
      ],
      [
        _, _, _, _, _,
        _, o, o, o, o,
        _, _, o, o, _,
        _, o, o, o, o,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'hotdog',
    color: 'red',
    // prettier-ignore
    layout: [
      [
        _, o, _, o, _,
        _, o, _, o, _,
        _, o, _, o, _,
        _, o, _, o, _,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, o, o, o,
        _, _, _, _, _,
        _, o, o, o, o,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'weird stair down',
    color: 'green',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, _, _, o, _,
        _, _, o, o, _,
        _, o, o, _, _,
        _, o, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, o, _, _,
        _, _, o, o, _,
        _, _, _, o, o,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'weird stair up',
    color: 'magenta',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, o, _, _, _,
        _, o, o, _, _,
        _, _, o, o, _,
        _, _, _, o, _,
      ],
      [
        _, _, _, _, _,
        _, _, _, o, o,
        _, _, o, o, _,
        _, o, o, _, _,
        _, _, _, _, _,
      ],
    ],
  },
  {
    name: 'rectangle',
    color: 'yellow',
    // prettier-ignore
    layout: [
      [
        _, _, _, _, _,
        _, o, o, o, o,
        _, o, o, o, o,
        _, o, o, o, o,
        _, _, _, _, _,
      ],
      [
        _, _, _, _, _,
        _, o, o, o, _,
        _, o, o, o, _,
        _, o, o, o, _,
        _, o, o, o, _,
      ],
    ],
  },
];

function makeTetrominoDict(tetrominos: Tetromino[]): StringDictionary<Tetromino> {
  const d: StringDictionary<Tetromino> = {};

  for (const tetromino of tetrominos) {
    d[tetromino.name] = tetromino;
  }

  return d;
}

function makeIndexDict(tetrominos: Tetromino[]): StringDictionary<number> {
  const d: StringDictionary<number> = {};

  for (const [i, tetromino] of tetrominos.entries()) {
    d[tetromino.name] = i + 1;
  }

  return d;
}

export const normalTetrominosByName = makeTetrominoDict(normalTetrominos);
export const attackTetrominosByName = makeTetrominoDict(attackTetrominos);
export const counterTetrominosByName = makeTetrominoDict(counterTetrominos);
export const overflowTetrominosByName = makeTetrominoDict(overflowTetrominos);
export const forbiddenTetrominosByName = makeTetrominoDict(forbiddenTetrominos);

export const normalNameToIdx = makeIndexDict(normalTetrominos);
export const attackNameToIdx = makeIndexDict(attackTetrominos);
export const counterNameToIdx = makeIndexDict(counterTetrominos);
export const overflowNameToIdx = makeIndexDict(overflowTetrominos);
export const forbiddenNameToIdx = makeIndexDict(forbiddenTetrominos);

export const phaseOneAttacks: string[] = ['big_I', 'd', 'b', 'u', 'ugh', 'hug'];

export const phaseTwoAttacks: string[] = ['big_I', 'd', 'b', 'u', 'ugh', 'hug', 'stairs', 'plus', 'amogus'];

export const phaseThreeAttacks: string[] = [
  'big_I',
  'd',
  'b',
  'u',
  'ugh',
  'hug',
  'stairs',
  'corner',
  'zigzag',
  'plus',
  'amogus',
];

export const phaseFourAttacks: string[] = [
  'big_I',
  'd',
  'b',
  'u',
  'ugh',
  'hug',
  'stairs',
  'corner',
  'zigzag',
  'big_s',
  'big_z',
  'plus',
  'amogus',
];
