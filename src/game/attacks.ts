export type CounterLevel = 1 | 2 | 3 | 4;
export const counterLevels: CounterLevel[] = [1, 2, 3, 4];
