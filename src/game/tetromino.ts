import type { ColorName } from '../colors';
import * as res from '../res';
import type { Coordinate, CoordinateLayout, StringDictionary } from '../types';
import { counterLevels } from './attacks';
import type { CounterLevel } from './attacks';
import { clockPhase } from './clock';
import {
  attackNameToIdx,
  attackTetrominos,
  attackTetrominosByName,
  counterTetrominos,
  normalNameToIdx,
  normalTetrominos,
  normalTetrominosByName,
  overflowNameToIdx,
  overflowTetrominos,
  overflowTetrominosByName,
  phaseFourAttacks,
  phaseOneAttacks,
  phaseThreeAttacks,
  phaseTwoAttacks,
} from './tetromino_layout';
import type { Tetromino } from './tetromino_layout';

type builderRet = LuaMultiReturn<[string, ColorName, Coordinate[][]]>;
type pieceBuilder = (which?: string) => builderRet;

function buildBuildPiece(
  resLayouts: CoordinateLayout[],
  tetrominoTable: Tetromino[],
  indexTable: StringDictionary<number>,
  randomizer: () => number
): pieceBuilder {
  return (which?: string): builderRet => {
    let which_idx: number;
    if (typeof which === 'string') {
      if (which in indexTable) {
        which_idx = indexTable[which];
      } else {
        which = undefined;
      }
    }
    if (typeof which === 'undefined') {
      // todo better piece randomization
      // will need to use timer maybe?
      which_idx = randomizer();
    }
    const piece: CoordinateLayout = maybeAddResource(resLayouts, which_idx, () => {
      const descr = tetrominoTable[which_idx - 1];
      return {
        name: descr.name,
        color: descr.color,
        rotations: buildRotations(descr.layout),
      };
    });

    return $multi(piece.name, piece.color, piece.rotations);
  };
}

export const build = buildBuildPiece(res.layouts.normal, normalTetrominos, normalNameToIdx, randomNormalPiece);
export const buildAttack = buildBuildPiece(res.layouts.attack, attackTetrominos, attackNameToIdx, randomAttackPiece);
export const buildOverflow = buildBuildPiece(
  res.layouts.overflow,
  overflowTetrominos,
  overflowNameToIdx,
  randomOverflowPiece
);

export function buildPhasedAttack(clock_phase: clockPhase): builderRet {
  const phase_table = { 1: phaseOneAttacks, 2: phaseTwoAttacks, 3: phaseThreeAttacks, 4: phaseFourAttacks }[
    clock_phase
  ];

  function randomPhased(): number {
    const name: string = phase_table[love.math.random(phase_table.length) - 1];
    return attackNameToIdx[name];
  }

  return buildBuildPiece(res.layouts.attack, attackTetrominos, attackNameToIdx, randomPhased)();
}

// buildCounter doesn't do random pieces, just needs a counter level
export function buildCounter(counter_level: CounterLevel): builderRet {
  const piece: CoordinateLayout = maybeAddResource(res.layouts.counter, counter_level, () => {
    const descr = counterTetrominos[counter_level - 1];
    return {
      name: descr.name,
      color: descr.color,
      rotations: buildRotations(descr.layout),
    };
  });
  return $multi(piece.name, piece.color, piece.rotations);
}

function maybeAddResource<T>(resTable: T[], idx: number, builder: () => T): T {
  if (!resTable[idx - 1]) {
    resTable[idx - 1] = builder();
  }
  return resTable[idx - 1];
}

function buildRotations(layout: boolean[][]): Coordinate[][] {
  const rotations: Coordinate[][] = [];
  for (const rotation of layout) {
    const squares = [];
    let x = -2;
    let y = -2;
    for (const present of rotation) {
      if (present) {
        table.insert(squares, { x: x, y: y });
      }

      x = x + 1;
      if (x > 2) {
        y = y + 1;
        x = -2;
      }
    }
    table.insert(rotations, squares);
  }

  return rotations;
}

export function preload_sprites(grid_size: number): void {
  for (const key in normalTetrominosByName) {
    const [name, color, rotations] = build(key);
    res.getPieceSprites(name, color, rotations, grid_size);
  }
  for (const key in attackTetrominosByName) {
    const [name, color, rotations] = buildAttack(key);
    res.getPieceSprites(name, color, rotations, grid_size);
  }
  for (const key in overflowTetrominosByName) {
    const [name, color, rotations] = buildOverflow(key);
    res.getPieceSprites(name, color, rotations, grid_size);
  }
  for (const i of counterLevels) {
    const [name, color, rotations] = buildCounter(i);
    res.getPieceSprites(name, color, rotations, grid_size);
  }
}

function randomNormalPiece(): number {
  return love.math.random(normalTetrominos.length);
}

function randomAttackPiece(): number {
  return love.math.random(attackTetrominos.length);
}

function randomOverflowPiece(): number {
  return love.math.random(overflowTetrominos.length);
}
