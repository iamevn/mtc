import { Player } from '../player';
import { PlayerState } from './playerstate';

export class GameOver extends PlayerState {
  constructor(player: Player) {
    super(player);
    this.name = 'gameover';
  }

  onEnter(): void {
    print('game over');
  }

  draw(): void {
    this.player.board.draw();
    this.player.piece.draw();
    this.player.drawScore();
    this.player.magicmeter.draw();
  }
}
