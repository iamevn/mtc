import { Player } from '../player';

/**
 * States for player to be in, states should have a name in their table
 *   and can implement onEnter, onExit, tick, checkInput, and draw
 *
 *   onExit gets called when changeState is called
 *   onEnter gets called before changeState returns
 *   tick gets called every tick (fixedish time frame)
 *   checkInput gets called every update (there may be multiple per tick)
 *   draw gets called every update (there may be multiple per tick)
 *
 *   state methods are called so that self is the Player instance
 *
 *   please don't call changeState in onExit, it won't do what you want
 */
const DEBUG_STATES = false;

export class PlayerState {
  name: string;
  player: Player;

  constructor(player: Player) {
    this.player = player;
    this.name = 'baseState';
  }

  onEnter(): void {
    if (DEBUG_STATES) {
      print('onEnter unimplemented in state ' + this.name);
    }
  }

  onExit(): void {
    if (DEBUG_STATES) {
      print('onExit unimplemented in state ' + this.name);
    }
  }

  tick(): void {
    if (DEBUG_STATES) {
      print('tick unimplemented in state ' + this.name);
    }
  }

  checkInput(): void {
    if (DEBUG_STATES) {
      print('checkInput unimplemented in state ' + this.name);
    }
  }

  draw(): void {
    if (DEBUG_STATES) {
      print('draw unimplemented in state ' + this.name);
    }
  }
}
