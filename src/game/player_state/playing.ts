import { Player } from '../player';
import { PlayerState } from './playerstate';

export class Playing extends PlayerState {
  constructor(player: Player) {
    super(player);
    this.name = 'playing';
  }

  tick(): void {
    const self = this.player;
    const player = this.player;
    const constants = player.constants;

    if (self.queued_moves.hard_drop) {
      self.queued_moves.hard_drop = null;
      while (self.piece.canMove(0, 1)) {
        self.piece.move(0, 1);
      }
      return self.lockPiece();
    }

    if (self.queued_moves.spin) {
      if (self.queued_moves.spin == 'clockwise') {
        self.piece.rotateClockwise();
      } else {
        self.piece.rotateCounterClockwise();
      }
      self.queued_moves.spin = null;
    }

    if (self.queued_moves.right) {
      if (self.piece.canMove(1, 0)) {
        self.piece.move(1, 0);
      }
      self.queued_moves.right = null;
    }

    if (self.queued_moves.left) {
      if (self.piece.canMove(-1, 0)) {
        self.piece.move(-1, 0);
      }
      self.queued_moves.left = null;
    }

    if (self.queued_moves.down) {
      self.queued_moves.down = null;
      if (self.piece.canMove(0, 1)) {
        self.piece.move(0, 1);
        self.ticks_until.gravity = this.player.current_gravity();
      } else {
        // pressing down locks
        return self.lockPiece();
      }
    }

    // tick tickers
    if (self.held_moves.right) {
      self.held_moves.right = self.held_moves.right - 1;
      if (self.held_moves.right <= 0) {
        if (self.piece.canMove(1, 0)) {
          self.piece.move(1, 0);
        }
        self.held_moves.right = constants.DAS_repeat;
      }
    }
    if (self.held_moves.left) {
      self.held_moves.left = self.held_moves.left - 1;
      if (self.held_moves.left <= 0) {
        if (self.piece.canMove(-1, 0)) {
          self.piece.move(-1, 0);
        }
        self.held_moves.left = constants.DAS_repeat;
      }
    }

    // set held_moves.left/right after shift tickers hit 0
    if (self.ticks_until.rightshift) {
      self.ticks_until.rightshift = self.ticks_until.rightshift - 1;
      if (self.ticks_until.rightshift <= 0) {
        self.held_moves.right = constants.DAS_repeat;
        self.ticks_until.rightshift = null;
      }
    }
    if (self.ticks_until.leftshift) {
      self.ticks_until.leftshift = self.ticks_until.leftshift - 1;
      if (self.ticks_until.leftshift <= 0) {
        self.held_moves.left = constants.DAS_repeat;
        self.ticks_until.leftshift = null;
      }
    }

    // handle helds down
    // handle gravity
    if (self.held_moves.down) {
      if (self.piece.canMove(0, 1)) {
        self.piece.move(0, 1);
      }
      self.ticks_until.gravity = this.player.current_gravity();
    } else {
      self.ticks_until.gravity = self.ticks_until.gravity - 1;
      if (self.ticks_until.gravity <= 0) {
        if (self.piece.canMove(0, 1)) {
          self.piece.move(0, 1);
        }
        self.ticks_until.gravity = this.player.current_gravity();
      }
    }
    // check for lock conditions
    if (!self.piece.canMove(0, 1)) {
      self.ticks_until.lock = self.ticks_until.lock || constants.lock_delay;
      self.ticks_until.lock = self.ticks_until.lock - 1;
      if (self.ticks_until.lock <= 0) {
        self.ticks_until.lock = null;
        return self.lockPiece();
      }
    }
    self.board.tick();
  }

  checkInput(): void {
    const self = this.player;
    const player = this.player;
    const constants = player.constants;

    if (self.input.pressed('down')) {
      self.queued_moves.down = true;
      self.held_moves.down = true;
    }

    if (self.input.pressed('left') && !self.input.down('right')) {
      self.queued_moves.right = null;
      self.queued_moves.left = true;
      self.held_moves.right = null;
      self.held_moves.left = null;
      self.ticks_until.rightshift = null;
      self.ticks_until.leftshift = self.ticks_until.leftshift || constants.DAS_delay;
    } else if (self.input.pressed('right') && !self.input.down('left')) {
      self.queued_moves.left = null;
      self.queued_moves.right = true;
      self.held_moves.right = null;
      self.held_moves.left = null;
      self.ticks_until.leftshift = null;
      self.ticks_until.rightshift = self.ticks_until.rightshift || constants.DAS_delay;
    } else if (
      (self.input.pressed('left') && self.input.down('right')) ||
      (self.input.pressed('right') && self.input.down('left'))
    ) {
      self.queued_moves.left = null;
      self.queued_moves.right = null;
      self.held_moves.left = null;
      self.held_moves.right = null;
      self.ticks_until.leftshift = null;
      self.ticks_until.rightshift = null;
    }

    if (!self.held_moves.left && self.input.down('left') && !self.input.down('right')) {
      self.ticks_until.leftshift = self.ticks_until.leftshift || constants.DAS_delay;
    }

    if (!self.held_moves.right && self.input.down('right') && !self.input.down('left')) {
      self.ticks_until.rightshift = self.ticks_until.rightshift || constants.DAS_delay;
    }

    // no repeat for spins, don't think there should be though
    if (self.input.pressed('clockwise_spin')) {
      self.queued_moves.spin = 'clockwise';
    }
    if (self.input.pressed('counterclockwise_spin')) {
      self.queued_moves.spin = 'counterclockwise';
    }

    if (self.input.pressed('up')) {
      self.queued_moves.hard_drop = true;
      self.ticks_until.leftshift = null;
      self.ticks_until.rightshift = null;
    }

    if (!self.input.down('down')) {
      self.held_moves.down = null;
    }

    if (!self.input.down('left')) {
      self.held_moves.left = null;
      self.ticks_until.leftshift = null;
    }

    if (!self.input.down('right')) {
      self.held_moves.right = null;
      self.ticks_until.rightshift = null;
    }
  }

  draw(): void {
    const player = this.player;

    player.board.draw();
    player.piece.draw();
    player.drawScore();
    player.drawCombo();
    player.magicmeter.draw();
  }
}
