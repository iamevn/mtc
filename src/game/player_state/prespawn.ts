import { Player } from '../player';
import { PlayerState } from './playerstate';

export class Prespawn extends PlayerState {
  constructor(player: Player) {
    super(player);
    this.name = 'prespawn';
  }

  onEnter(): void {
    this.player.held_moves = {};
    const mm = this.player.magicmeter;
    if (mm.isFull()) {
      this.player.board.magicMeterCollapse(mm.blockHeight());
      this.player.bag.queue_I();
      print('clear meter');
      mm.levelUp();
    }

    if (this.player.board.allClear()) {
      print('full clear');
      mm.levelDown();
    }
  }

  tick(): void {
    const self = this.player;
    const player = this.player;
    const constants = player.constants;
    const states = player.states;
    const ticks_until = player.ticks_until;

    ticks_until.spawn -= 1;
    if (ticks_until.spawn <= 0) {
      if (player.spawnPiece()) {
        player.changeState(states.Playing);
      } else {
        player.changeState(states.GameOver);
      }
    } else if (constants.precharge_das) {
      // preload DAS during ARE time
      if (ticks_until.rightshift) {
        ticks_until.rightshift -= 1;
        if (ticks_until.rightshift <= 0) {
          self.held_moves.right = constants.DAS_repeat;
          delete ticks_until.rightshift;
        }
      }
      if (ticks_until.leftshift) {
        ticks_until.leftshift -= 1;
        if (ticks_until.leftshift <= 0) {
          self.held_moves.left = constants.DAS_repeat;
          delete ticks_until.leftshift;
        }
      }
    }

    player.board.tick();
  }

  checkInput(): void {
    const self = this.player;
    const player = this.player;
    const constants = player.constants;
    const states = player.states;
    const ticks_until = player.ticks_until;

    // charge DAS during ARE/line clear time
    // not authentic to the original game but feels good imo
    if (constants.precharge_das) {
      // should we queue up a left/right when left/right pressed?
      if (!self.held_moves.left && self.input.down('left') && !self.input.down('right')) {
        ticks_until.leftshift = ticks_until.leftshift || constants.DAS_delay;
      }
      if (!self.held_moves.right && self.input.down('right') && !self.input.down('left')) {
        ticks_until.rightshift = ticks_until.rightshift || constants.DAS_delay;
      }
    }
  }

  draw(): void {
    const player = this.player;

    player.board.draw();
    player.drawScore();
    player.drawCombo();
    player.magicmeter.draw();
  }
}
