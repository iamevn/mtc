import type { Color } from '../colors';
import { BaseObject } from '../types';
import type { Coordinate } from '../types';
import type { Board } from './board';

type meterHeight = 2 | 4 | 5 | 6 | 7;

export class MagicMeter extends BaseObject {
  public level: number;
  public charges: number;
  public ui: {
    gridsize: number;
    margin: number;
    colors: {
      bg: Color;
      fg: Color;
    };
    bottom_left: Coordinate;
  };
  public board: Board;
  public _charges_per_block: number;
  public _block_heights: meterHeight[];

  constructor(board: Board) {
    super();
    this.level = 2;
    this.charges = 0;

    const grid_size = board.grid.size;
    const margin = 1;
    const grid_height = board.grid.px_height;

    this.ui = {
      gridsize: grid_size,
      margin: margin,
      colors: {
        bg: [0.2, 0.3, 0.3],
        fg: [0.5, 0.6, 0.9],
      },
      bottom_left: {
        x: -grid_size - margin,
        y: grid_height,
      },
    };

    this.board = board;
    this._charges_per_block = 8;
    this._block_heights = [2, 4, 5, 6, 7];
  }

  public levelUp(): void {
    this.charges = 0;
    if (this.level < 5) {
      this.level += 1;
    }
  }

  public levelDown(): void {
    if (this.level > 1) {
      this.level -= 1;
      // reset charges when leveling down, not resetting if already at lowest level
      this.charges = 0;
    }
  }

  public blockHeight(): 2 | 4 | 5 | 6 | 7 {
    return this._block_heights[this.level - 1];
  }

  public isFull(): boolean {
    return this.charges == this.blockHeight() * this._charges_per_block;
  }

  public addCharge(): boolean {
    if (!this.isFull()) {
      this.charges += 1;
    }
    return this.isFull();
  }

  public draw(): void {
    const bg_height = this.blockHeight() * this.ui.gridsize;
    const fill_height = (this.charges / this._charges_per_block) * this.ui.gridsize;

    love.graphics.push('all');
    love.graphics.setColor(...this.ui.colors.bg);
    love.graphics.rectangle(
      'fill',
      this.ui.bottom_left.x,
      this.ui.bottom_left.y - bg_height,
      this.ui.gridsize,
      bg_height
    );

    if (this.charges >= 0) {
      love.graphics.setColor(...this.ui.colors.fg);
      love.graphics.rectangle(
        'fill',
        this.ui.bottom_left.x,
        this.ui.bottom_left.y - fill_height,
        this.ui.gridsize,
        fill_height
      );
    }

    love.graphics.pop();
  }
}
