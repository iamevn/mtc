import type { Input } from '../types';
import { BaseObject } from '../types';
import { Bag } from './bag';
import { Clock } from './clock';
import { Player } from './player';

export class GameController extends BaseObject {
  left_player: Player;
  right_player?: Player;
  clock: Clock;
  bag: Bag;

  constructor(input: Input, input2?: Input) {
    super();
    const grid_size = 16;
    this.bag = new Bag(grid_size);
    this.clock = new Clock();

    this.left_player = new Player(input, this.bag, this.clock);
    if (input2) {
      this.right_player = new Player(input2, this.bag, this.clock);
      this.left_player.opponent = this.right_player;
      this.right_player.opponent = this.left_player;
    }
  }

  update(dt: number): void {
    this.bag.update(dt);
    this.left_player.update(dt);
    this.right_player?.update(dt);
    this.clock.update(dt);
  }

  tick(): void {
    this.bag.tick();
    this.left_player.tick();
    this.right_player?.tick();
    this.clock.tick();
  }

  draw(): void {
    love.graphics.push('all');
    love.graphics.translate(50, 60);
    love.graphics.scale(1.5);
    this.left_player.draw();
    love.graphics.pop();

    if (this.right_player) {
      love.graphics.push('all');
      love.graphics.translate(500, 60);
      love.graphics.scale(1.5);
      this.right_player.draw();
      love.graphics.pop();
    }

    love.graphics.push('all');
    love.graphics.translate(360, 30);
    this.clock.draw();

    love.graphics.translate(-8, 40);
    this.bag.draw();
    love.graphics.pop();
  }
}
