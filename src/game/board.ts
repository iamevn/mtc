import type { SpriteBatch } from 'love.graphics';

import { Array2DZ } from '../array2d';
import { BaseObject } from '../types';
import type { Bag } from './bag';
import { Block } from './block';
import type { Piece } from './piece';
import { Queue } from './queue';
import * as tetromino from './tetromino';

import res = require('../res');

// Board
// contains logic for one player's board

export class Board extends BaseObject {
  grid: {
    size: number;
    width: number;
    height: number;
    px_width: number;
    px_height: number;
    bg: SpriteBatch;
  };
  blocks: Array2DZ<Block>;
  queue: Queue;
  toClear?: number[];
  clearDelay?: number;

  constructor(bag: Bag) {
    super();
    const [gs, gw, gh] = [16, 10, 20];
    this.grid = {
      size: gs,
      width: gw,
      height: gh,
      px_width: gw * gs,
      px_height: gh * gs,

      bg: love.graphics.newSpriteBatch(res.block_img),
    };
    this.grid.bg.setColor(0.4, 0.4, 0.4, 0.2);

    for (const y of $range(0, this.grid.height - 1)) {
      if (y == 2) {
        this.grid.bg.setColor(0.9, 0.9, 0.9, 0.2);
      }
      for (const x of $range(0, this.grid.width - 1)) {
        this.grid.bg.add(x * this.grid.size, y * this.grid.size);
      }
    }

    this.blocks = new Array2DZ(this.grid.width, this.grid.height);

    tetromino.preload_sprites(this.grid.size);

    this.queue = new Queue(this, bag);
  }

  occupied(x: number, y: number): boolean {
    return !this.blocks.inBounds(x, y) || this.blocks.isFilled(x, y);
  }

  open(x: number, y: number): boolean {
    return this.blocks.inBounds(x, y) && this.blocks.isEmpty(x, y);
  }

  lock(piece: Piece): void {
    for (const coord of piece.squareCoords()) {
      this.blocks.put(coord.x, coord.y, new Block(coord.x, coord.y, piece.color, this));
    }
  }

  checkClears(): number[] {
    const clears: number[] = [];
    for (const y of $range(0, this.grid.height - 1)) {
      const row = this.blocks.getRow(y);
      let allFull = true;
      for (const x of $range(0, this.grid.width - 1)) {
        if (row[x] == null) {
          allFull = false;
        }
      }
      if (allFull) {
        table.insert(clears, y);
      }
    }
    return clears;
  }

  magicMeterCollapse(collapse_level: number): void {
    print('collapsing');
    // TODO: animate
    // condense columns
    const columns: Block[][] = [];
    for (const x of $range(0, this.blocks.width - 1)) {
      const column = this.blocks.getColumn(x);
      const squashed: Block[] = [];
      const bottom_row = this.blocks.height - 1;
      let squashed_y = bottom_row;
      for (const y of $range(bottom_row, 0, -1)) {
        if (column[y]) {
          const block = column[y];
          squashed.push(block);
          block.posn.y = squashed_y;
          squashed_y -= 1;
        }
      }
      columns.push(squashed);
    }

    // find shortest column
    let shortest_column = 0;
    for (const [x, column] of columns.entries()) {
      if (column.length < columns[shortest_column].length) {
        shortest_column = x;
      }
    }

    // remove shortest column
    columns[shortest_column] = [];

    // removes lines above collapse_level
    for (const column of columns) {
      while (column.length > collapse_level) {
        column.pop();
      }
    }

    // put pieces back into board
    this.blocks.clear();
    for (const column of columns) {
      for (const block of column) {
        this.blocks.put(block.posn.x, block.posn.y, block);
      }
    }

    // TODO: generate (6?) I pieces
  }

  allClear(): boolean {
    if (this.toClear) {
      const clearRows: boolean[] = [];
      for (const row of this.toClear) {
        clearRows[row - 1] = true;
      }

      for (const [, block] of this.blocks.entries()) {
        if (!clearRows[block.posn.y - 1]) {
          return false;
        }
      }
      return true;
    } else {
      return false;
    }
  }

  tick(): void {
    if (this.clearDelay) {
      this.clearDelay -= 1;
      if (this.clearDelay <= 0) {
        for (const row of this.toClear) {
          this.clearRow(row);
        }
        delete this.clearDelay;
        delete this.toClear;
      }
    }

    for (const [, block] of this.blocks.entries()) {
      block.tick();
    }
  }

  scheduleClear(row: number, delay?: number): void {
    this.clearDelay = delay || 12;
    this.toClear = this.toClear || [];
    table.insert(this.toClear, row);

    for (const [, block] of pairs(this.blocks.getRow(row))) {
      block.flash(this.clearDelay);
    }
  }

  clearRow(row: number): void {
    if (row == 0) {
      for (const x of $range(0, this.grid.width - 1)) {
        this.blocks.remove(x, row);
      }
    } else {
      for (const y of $range(row, 1, -1)) {
        for (const x of $range(0, this.grid.width - 1)) {
          const block = this.blocks.put(x, y, this.blocks.remove(x, y - 1));
          if (block) {
            block.posn.x = x;
            block.posn.y = y;
          }
        }
      }
    }
  }

  draw(): void {
    love.graphics.draw(this.grid.bg);
    for (const [, block] of this.blocks.entries()) {
      block.draw();
    }

    this.queue.draw();
  }
}
