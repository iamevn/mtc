import { GameController } from './game/gamecontroller';
import type { Input } from './types';

import baton = require('./lib/baton/baton');

const two_player = false;
let input: Input = null;
let input2: Input = null;
let game: GameController = null;

let tick = 0;
const TICKRATE = 1 / 60;

love.load = (): void => {
  input = baton.new({
    controls: {
      left: ['key:a', 'axis:leftx-', 'button:dpleft'],
      right: ['key:d', 'axis:leftx+', 'button:dpright'],
      up: ['key:w', 'axis:lefty-', 'button:dpup'],
      down: ['key:s', 'axis:lefty+', 'button:dpdown'],
      clockwise_spin: ['key:x', 'button:a'],
      counterclockwise_spin: ['key:z', 'button:b'],
      debug_1: ['key:1'],
      debug_2: ['key:2'],
      debug_3: ['key:3'],
      debug_4: ['key:4'],
      debug_5: ['key:5'],
    },
  });

  input2 = baton.new({
    controls: {
      left: ['key:left'],
      right: ['key:right'],
      up: ['key:up'],
      down: ['key:down'],
      clockwise_spin: ['key:kp0'],
      counterclockwise_spin: ['key:kp1'],
      debug_1: [],
      debug_2: [],
      debug_3: [],
      debug_4: [],
      debug_5: [],
    },
  });

  require('res');

  if (two_player) {
    game = new GameController(input, input2);
  } else {
    game = new GameController(input);
  }
};

love.update = (dt: number): void => {
  game.update(dt);
  tick = tick + dt;
  while (tick >= TICKRATE) {
    tick -= TICKRATE;
    game.tick();
  }
};

love.draw = (): void => {
  game.draw();
};
