import { KeyConstant, getKeyFromScancode } from 'love.keyboard';

interface ControlMap {
  [Key: string]: string[];
}

interface AxisPairs {
  [Key: string]: string[];
}

declare interface Config {
  controls?: ControlMap;
  pairs?: AxisPairs;
  deadzone?: number;
  squareDeadzone?: boolean;
}

declare interface Player {
  config: Config;
  update(): void;
  get(control: string): number | LuaMultiReturn<[number, number]>;
  down(control: string): boolean;
  pressed(control: string): boolean;
  released(control: string): boolean;
  getActiveDevice(): 'kbm' | 'joy' | 'none';
}

declare let baton: {
  new: (this: void, config: Config) => Player;
  Player: Player;
  Config: Config;
};
export = baton;
