let random_state = 0;

(_G as any).love = {
  math: {
    random: (upper: number) => {
      random_state = (random_state % upper) + 1;
      return random_state;
    },
  },
  graphics: {
    newImage: (filename: string) => {
      // print('new image ' + filename);
      return {};
    },
    newSpriteBatch: (image: string) => {
      // print('new sprite batch ' + image);
      return {};
    },
  },
};
