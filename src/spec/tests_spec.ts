import 'love_mocks';

import * as tetromino from '../game/tetromino';
import * as tetromino_layout from '../game/tetromino_layout';

describe('tetromino', () => {
  it('builds random normal pieces without errors', () => {
    for (const _ of $range(1, 1000)) {
      const [name, color, rotations] = tetromino.build();
      assert.not_nil(name);
    }
  });

  it('builds random attack pieces without errors', () => {
    for (const _ of $range(1, 1000)) {
      const [name, color, rotations] = tetromino.buildAttack();
      assert.not_nil(name);
    }
  });

  it('builds random overflow pieces without errors', () => {
    for (const _ of $range(1, 1000)) {
      const [name, color, rotations] = tetromino.buildOverflow();
      assert.not_nil(name);
    }
  });

  describe('counter pieces', () => {
    it('builds level 1 counter pieces correctly', () => {
      const [name, color, rotations] = tetromino.buildCounter(1);
      assert.equal(name, 'small_counter');
      assert.equal(rotations.length, 1, "squares don't rotate");
      assert.equal(rotations[0].length, 4, '2x2 square has 4 spaces');
    });
    it('builds level 2 counter pieces correctly', () => {
      const [name, color, rotations] = tetromino.buildCounter(2);
      assert.equal(name, 'medium_counter');
      assert.equal(rotations.length, 1, "squares don't rotate");
      assert.equal(rotations[0].length, 9, '3x3 square has 9 spaces');
    });
    it('builds level 3 counter pieces correctly', () => {
      const [name, color, rotations] = tetromino.buildCounter(3);
      assert.equal(name, 'large_counter');
      assert.equal(rotations.length, 1, "squares don't rotate");
      assert.equal(rotations[0].length, 16, '4x4 square has 16 spaces');
    });
    it('builds level 4 counter pieces correctly', () => {
      const [name, color, rotations] = tetromino.buildCounter(4);
      assert.equal(name, 'giant_counter');
      assert.equal(rotations.length, 1, "squares don't rotate");
      assert.equal(rotations[0].length, 25, '5x5 square has 25 spaces');
    });
  });

  describe('piece_builder', () => {
    it('builds I piece when requested', () => {
      const [name, color, rotations] = tetromino.build('I');
      assert.equal(name, 'I');
    });

    it('builds any piece when requested', () => {
      const names: Set<string> = new Set();
      for (const piece of tetromino_layout.normalTetrominos) {
        names.add(piece.name);
      }

      for (const to_build of names) {
        const [name, color, rotations] = tetromino.build(to_build);
        assert.equal(name, to_build);
      }
    });
  });
});

describe('tetromino_layout', () => {
  it('has unique names for every tetromino', () => {
    const names: Set<string> = new Set();
    const collisions: Set<string> = new Set();
    const layout_groups = [
      tetromino_layout.normalTetrominos,
      tetromino_layout.attackTetrominos,
      tetromino_layout.counterTetrominos,
      tetromino_layout.overflowTetrominos,
      tetromino_layout.forbiddenTetrominos,
    ];
    for (const group of layout_groups) {
      for (const layout of group) {
        if (names.has(layout.name)) {
          collisions.add(layout.name);
        }
        names.add(layout.name);
      }
    }
    assert.is_equal(0, collisions.size, `overlapping names: ${[...collisions].map((s) => `"${s}"`).join(', ')}`);
  });
});
