// colors: https://color.gd/?-bfbdbf-e3001e-ff6a00-2617ff-41faf7-fa02ee-00d92b-fbff00-ffb3c6-9d00f2-6df2ca-fafafa;
export type Color = [number, number, number];

export type ColorName =
  | 'gray'
  | 'red'
  | 'orange'
  | 'blue'
  | 'cyan'
  | 'magenta'
  | 'green'
  | 'yellow'
  | 'peach'
  | 'purple'
  | 'teal'
  | 'white';

export const Colors = new LuaTable<ColorName, Color>();
Colors.set('gray', [191 / 255, 189 / 255, 191 / 255]);
Colors.set('red', [227 / 255, 0 / 255, 30 / 255]);
Colors.set('orange', [255 / 255, 106 / 255, 0 / 255]);
Colors.set('blue', [38 / 255, 23 / 255, 255 / 255]);
Colors.set('cyan', [65 / 255, 250 / 255, 247 / 255]);
Colors.set('magenta', [250 / 255, 2 / 255, 238 / 255]);
Colors.set('green', [0 / 255, 217 / 255, 43 / 255]);
Colors.set('yellow', [251 / 255, 255 / 255, 0 / 255]);
Colors.set('peach', [255 / 255, 179 / 255, 198 / 255]);
Colors.set('purple', [157 / 255, 0 / 255, 242 / 255]);
Colors.set('teal', [109 / 255, 242 / 255, 202 / 255]);
Colors.set('white', [250 / 255, 250 / 255, 250 / 255]);

export const getColor = (name: ColorName): Color => {
  return Colors.get(name);
};
