import type { ColorName } from './colors';
import type { Player } from './lib/baton/baton';

export type Coordinate = { x: number; y: number };
export type Rotation = Coordinate[];
export type Input = typeof Player;

export interface CoordinateLayout {
  name: string;
  color: ColorName;
  rotations: Rotation[];
}

export interface NumberDictionary<T> {
  [key: number]: T;
}

export interface StringDictionary<T> {
  [Key: string]: T;
}

export class BaseObject {
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  tick(): void {}
  // eslint-disable-next-line @typescript-eslint/no-empty-function,@typescript-eslint/no-unused-vars
  update(dt: number): void {}
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  draw(): void {}
}
