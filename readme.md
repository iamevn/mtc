# What this is

There's [an n64/playstation/arcade block puzzle game](https://tetris.wiki/Magical_Tetris_Challenge)
which I am really fond of. This is a reimplementation of its unique game mode.

The basic idea is that attacks send "magical pieces" over instead of your usual garbage rows. These
are larger, more difficult to place pieces than regular tetrominoes. If you counter them while they
are in your queue, they get sent back to your opponent as squares. Initially this will be a normal
2x2 square but if those get countered they become 3x3 squares which can be countered again to make
4x4 and finally 5x5 squares. Combine this style of attack with a lack of a hold system and you have
a much messier game than most block puzzle game modes. I'd love for more people to try it.


# Scope

I'm aiming for the initial version to be fairly simple graphically but feel good to play and have
both local multiplayer and some simple AI.

I'd like for this to be as accurate to the original as I can in timing and mechanics.

Once that's done, I will polish things up (music, animations) and look into online multiplayer.
It'd be really cool if I can get it running on the web and on the Switch or 3DS through homebrew.

Built with [LÖVE 11.3](https://love2d.org/) using [TypeScriptToLua](https://typescripttolua.github.io/).

# Dev instructions

* Install [yarn](https://classic.yarnpkg.com/en/docs/install) and [love2d](https://love2d.org/)
* Clone this repo, run `yarn install` in the base of this repo (where this file is)
* Run `yarn fixlint` to lint and `yarn build` to compile. This will create a `game/` folder in this project.
* Either run `love game` or `yarn start` to start the game.
* To test, use [busted](https://olivinelabs.com/busted/) from within the `game/` folder after building.
  * I had trouble getting busted installed on windows. The easiest way I could figure out was to use [hererocks](https://github.com/mpeterv/hererocks):
    * `hererocks C:\lua51 -l 5.1 -r latest`
    * add `C:\lua51\bin` to my path
    * `luarocks install busted`
  * test command (bash): `yarn fixlintbuild && busted --verbose --shuffle --directory game`
    * on windows (powershell): `yarn fixlintbuild; if ($?) { busted --verbose --shuffle --directory game }`

The `raw/` folder is copied over as-is when building the game. This is mainly intended to be used for resources.
The `src/` folder contains typescript and lua files that will be compiled and copied over respectively when building the game.

